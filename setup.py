import os
from setuptools import setup

# Starfinder
# Find observable objects in your night sky.

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "starfinder",
    version = "0.1.0",
    description = "Help observe the night sky",
    author = "Johan Nestaas",
    author_email = "johannestaas@gmail.com",
    license = "GPLv3+",
    keywords = "Astrophotography DSLR Imaging Open-Source astronomy telescope",
    url = "https://bitbucket.org/johannestaas/starfinder",
    packages=['starfinder'],
    package_dir={'starfinder': 'starfinder'},
    long_description=read('README.md'),
    classifiers=[
        #'Development Status :: 1 - Planning',
        #'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',
        #'Development Status :: 6 - Mature',
        #'Development Status :: 7 - Inactive',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Other Audience',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Scientific/Engineering :: Astronomy',
        'Topic :: Scientific/Engineering :: Visualization',
        'Topic :: Scientific/Engineering :: Image Recognition',
        'Topic :: Multimedia :: Graphics :: Capture :: Digital Camera',
        'Environment :: Console',
        'Environment :: X11 Applications :: Qt',
        'Environment :: MacOS X',
        'Environment :: Win32 (MS Windows)',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
        'Topic :: Utilities',
    ],
    install_requires=[
        'pyephem>=3.7.5.2',
    ],
    entry_points = {
        'console_scripts': [
            'starfinder = starfinder.bin:starfinder',
        ],
    },
    package_data = {
        'starfinder': ['catalog/*.edb'],
    },
    include_package_data = True,
)
