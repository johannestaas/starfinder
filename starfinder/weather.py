#!/usr/bin/env python

FORECAST_10DAY_URL = 'http://api.wunderground.com/api/%(api_key)s/hourly10day/q/%(lat)s,%(lon)s.json'
