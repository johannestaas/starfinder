#!/usr/bin/env python
import os
import sys
from math import pi
from datetime import datetime, timedelta
import ephem
import ephem.stars
import re

RE_MESSIER = re.compile(r'(?:^|\s+)M(\d+)(?:$|\s+)')
RE_NGC = re.compile(r'(?:^|\s+)NGC(\d+)(?:$|\s+)')

class Catalog(object):
    MESSIER = 1
    NGC = 2

TYPE_NAME = {
    'G': 'galaxy',
    'O': 'open cluster',
    'C': 'globular cluster',
    'N': 'nebula',
    'P': 'planetary nebula',
    'R': 'supernova remnant',
    'U': 'nebula',
}

def read_db(lines, err_out):
    catalog = {}
    for line in lines:
        lin = line.strip()
        if not lin:
            continue
        if lin.startswith('#') or lin.startswith('*'):
            continue
        try:
            m = ephem.readdb(lin)
            name = m.name
            mess_match = RE_MESSIER.search(name)
            ngc_match = RE_NGC.search(name)
            if mess_match:
                name = (int(mess_match.group(1)), Catalog.MESSIER)
            elif ngc_match:
                name = (int(ngc_match.group(1)), Catalog.NGC)
            else:
                name = (name, None)
            catalog[name] = m
        except ValueError as e:
            print 'Exception reading edb file: %s' % e
            print 'Reading line: %s' % line
            if err_out:
                raise
    return catalog

CATALOG = {}
CATALOG_DIR = os.path.join(
    os.path.abspath(os.path.dirname(__file__)), 'catalog')

def make_catalog(err_out=True):
    global_catalog = {}
    for catalog in os.listdir(CATALOG_DIR):
        cat_path = os.path.join(CATALOG_DIR, catalog)
        name = os.path.splitext(catalog)[0].lower()
        with open(cat_path) as cat_f:
            lines = cat_f.readlines()
        try:
            global_catalog[name] = read_db(lines, err_out)
        except ValueError as e:
            print 'Exception reading from %s' % cat_path
            print 'Either remove it, or fix the line with the syntax error.'
            if err_out:
                raise
    return global_catalog


def get_star_names():
    for star in ephem.stars.db.split("\n"):
        name = star.split(",")[0]
    yield name

def out_obj(obj, ind=''):
    def printind(s):
        print '%s%s' % (ind, s)
    printind('%s' % obj.name)
    for attr in ['az', 'alt', 'mag']:
        printind('%10s: %s' % (attr, getattr(obj, attr))) 
    try:
        printind('%10s: %s' % ('type', TYPE_NAME[obj._class]))
    except AttributeError:
        pass
    except KeyError as e:
        printind('%10s: %s' % ('type', obj._class))

def read_catalog_from_obs(global_catalog, obs,
    altitude_threshold=15.0,
    magnitude_threshold=10.0,
    whitelist=[]):
    ht_th = (altitude_threshold / 180.) * pi
    mag_th = magnitude_threshold
    for cname, catalog in sorted(global_catalog.items()):
        if whitelist and cname not in whitelist:
            continue
        print cname
        for name_typ, obj in sorted(catalog.items()):
            name, typ = name_typ
            obj.compute(obs)
            if obj.alt < ht_th:
                continue
            if obj.mag > mag_th:
                continue
            out_obj(obj, ind='\t')

def main():
    global_catalog = make_catalog()
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--catalogs', '-c', nargs='+',
        help='Choose catalogs to check, from: %s' % str(
        global_catalog.keys()))
    parser.add_argument('--time-delta', '-t', type=float, default=0.0,
        help='time in hours from now')
    parser.add_argument('--city', '-C',
        help='City of origin', default='San Francisco')
    parser.add_argument('--altitude-threshold', '-a',
        default=15.0, type=float,
        help='minimum degrees of altitude')
    parser.add_argument('--magnitude-threshold', '-m',
        default=10.0, type=float,
        help='maximum magnitude (goes in reverse)')
    parser.add_argument('--lat', default=None, help='latitude')
    parser.add_argument('--lon', default=None, help='longitude')
    args = parser.parse_args()
    if not (args.lat and args.lon):
        obs = ephem.city(args.city)
    else:
        obs = ephem.Observer()
        obs.lon = args.lon
        obs.lat = args.lat
    obs.date = datetime.utcnow() + timedelta(0, args.time_delta*60*60)
    read_catalog_from_obs(global_catalog, obs,
        whitelist=args.catalogs,
        altitude_threshold=args.altitude_threshold,
        magnitude_threshold=args.magnitude_threshold)

if __name__ == '__main__':
    main()
